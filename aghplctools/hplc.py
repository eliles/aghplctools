import os
import time
import pylab as pl
from hein_utilities.files import Watcher
from hein_utilities.misc import find_nearest
from unithandler.base import UnitFloat
from .config import CHEMSTATION_DATA_PATH

# todo replace once UnitFloat is fixed for si prefix
# todo add ability to prevent automatic scaling
_min = UnitFloat(1.)
_min.unit = 'min'


class HPLCTarget(object):
    def __init__(self, wavelength, retention_time, name=None, wiggle=0.2):
        """
        A data storage class for tracking an HPLC retention target.

        :param float wavelength: wavelength to track the target on
        :param float retention_time: retention time to look for the target
        :param str name: convenience name
        :param float wiggle: wiggle value in minutes for finding the target around the retention_time (the window will
            be [retention_time-wiggle, retention_time+wiggle])
        """
        # todo track multiple wavelengths for a single target
        # todo create an overall manager to facilitate retrieval of target groups (e.g. grouped by wavelength)
        self.name = name
        self.wavelength = UnitFloat(wavelength, 'm', 'n', 'n')
        self.retention_time = retention_time * _min
        self.wiggle = wiggle * _min
        self.times = []  # tracks timepoints (e.g. reaction times)
        self.areas = []  # tracks peak areas
        self.widths = []  # tracks peak widths
        self.heights = []  # tracks peak heights

    def __repr__(self):
        return f'{self.__class__.__name__}({self.name})'

    def __str__(self):
        return f'{self.__class__.__name__}({self.name}, {self.wavelength}, {self.retention_time})'

    def __getitem__(self, item):
        return self.retrieve_index(item)

    def add_value(self, area, width=0., height=0., timepoint=None):
        """
        Adds a value to the tracker lists.

        :param float area: area to add (required)
        :param float width: width to add (optional)
        :param float height: height to add (optional)
        :param float timepoint: timepoint to use (if None, the current time will be called)
        """
        if timepoint is None:  # create timepoint if None
            timepoint = time.time()
        self.times.append(timepoint)  # append timepoint
        self.areas.append(area)  # append area
        # store width and height
        self.widths.append(width)
        self.heights.append(height)

    def add_from_pulled(self, signals, timepoint=None):
        """
        Retrieves values from the output of the pull_hplc_area function and stores them in the instance.

        :param dict signals: output dictionary from pull_hplc_area
        :param float timepoint: timepoint to save (if None, the current time will be retrieved)
        :return: area, height, width, timepoint
        :rtype: tuple
        """
        # initial values for area, height, and width
        area = 0.
        height = 0.
        width = 0.
        selwl = find_nearest(  # look for the tracked wavelength in the signals
            signals,
            self.wavelength,
            1.
        )
        if selwl is not None:  # if the target wavelength
            selret = find_nearest(  # look for the retention time
                signals[selwl],
                self.retention_time,
                self.wiggle,
            )
            if selret is not None:  # if retention time is present, retrieve values
                area = signals[selwl][selret]['Area']
                height = signals[selwl][selret]['Height']
                width = signals[selwl][selret]['Width']
        self.add_value(  # store retrieved values
            area,
            width,
            height,
            timepoint,
        )
        return area, height, width, timepoint

    def retrieve_index(self, index):
        """
        Retrieves the values of the provided index.

        :param index: pythonic list index
        :return: {area, width, height, timepoint}
        :rtype: dict
        """
        try:
            return {
                'area': self.areas[index],
                'width': self.widths[index],
                'height': self.heights[index],
                'timepoint': self.times[index],
            }
        except IndexError:
            raise IndexError(f'The index {index} is beyond the length of the {self.__repr__()} object'
                             f' ({len(self.times)}')

    def retrieve_timepoint(self, timepoint):
        """
        Retrieves the values of the provided timepoint.

        :param float timepoint: time point to retrieve
        :return: {area, width, height, timepoint}
        :rtype: dict
        """
        return self.retrieve_index(
            self.times.index(  # index the timepoint
                find_nearest(  # find the nearest timepoint to the specified value (avoid floating point errors)
                    self.times,
                    timepoint,
                    0.001,
                )
            )
        )


def acquiring_filename():
    """
    Retrieves the path name of the next Agilent HPLC acquisition (from acquiring.txt)

    :return: file being currently acquired
    :rtype: str
    """
    with open(acqwatch.contents[0], 'r', encoding='utf-16') as acquiring:
        try:
            datafile = acquiring.readline().split('|')[1].strip()  # current data file
        # datafile = acquiring.readline().split(':')[1].strip()  # current data file
        except IndexError:
        # if len(datafile) == 0:  # first acquisition has an extra line, subsequent ones dont
            datafile = acquiring.readline().split('|')[1].strip()
            # datafile = acquiring.readline().split(':')[1].strip()  # current data file
        path = acqwatch.find_subfolder()[0]  # active sequence directory
        if datafile.endswith('.D') is False:  # catch in case it didn't point to a data file
            raise ValueError(f'ACQUIRING.TXT did not point to a data file (retrieved: {datafile})')
        datawatch.path = (
            os.path.join(  # set the datawatch path
                path,
                datafile
            )
        )
        return datafile


def plot(yvalues, xvalues=None, xlabel='injection #', ylabel=None, hline=None):
    """
    plots one set of values
    :param yvalues:  list of y values
    :param xvalues: list of x values (optional)
    :param xlabel: label for x
    :param ylabel: label for y
    :param hline: plot a horizontal line at this value if specified
    :return:
    """
    pl.clf()
    pl.close()
    fig = pl.figure()
    ax = fig.add_subplot(111)
    if xvalues is not None:
        ax.plot(xvalues, yvalues)
    else:
        ax.plot(yvalues)
    if hline is not None:
        ax.axhline(hline, color='r')
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    pl.show()


def stackedplot(rets, xlabel='injection #'):
    """
    Creates a stacked plot for the dictionary generated by pull_hplc_data_from_folder
    :param rets: dictionary of retetion times
    :param xlabel: optional changing of x label
    """
    pl.clf()
    pl.close()

    times = list(rets.keys())  # retention times
    targets = rets[times[0]].keys()  # target keys

    fig, ax = pl.subplots(len(targets), 1)  # create subplot stack
    for ind, val in enumerate(targets):
        for time in times:  # plot each time
            ax[ind].plot(
                rets[time][val],
                label='%.3f min' % time,
            )

        if ind != len(targets) - 1:  # remove x values if not the last subplot
            ax[ind].set_xticklabels([])
        ax[ind].set_ylabel(val)

    if xlabel is not None:
        ax[ind].set_xlabel(xlabel)
    handles, labels = ax[ind].get_legend_handles_labels()  # retrieve legend values
    ax[ind].legend(handles, labels)  # show legend
    pl.show()  # show


if CHEMSTATION_DATA_PATH is not None:
    datawatch = Watcher(  # create watcher for HPLC output files
        CHEMSTATION_DATA_PATH,
        'Report.TXT'
    )
    acqwatch = Watcher(  # watcher for acquiring text file
        CHEMSTATION_DATA_PATH,
        'ACQUIRING.TXT'
    )
else:
    datawatch = None
    acqwatch = None


def find_max_area(signals):
    """
    Returns the wavelength and retention time corresponding to the maximum area in a set of HPLC peak data.

    :param dict signals: dict[wavelength][retention time (float)][width/area/height]
    :return:
    """
    """
    example of signals: {wavelength {retention time { 'width': ,'area': ,'height':}}}
                        {
                        210.0: {0.435: {'width': 0.0599, 'area': 2820.54077, 'height': 750.42493}}, 
                        230.0: {0.435: {'width': 0.0576, 'area': 585.83862, 'height': 162.34517}}, 
                        254.0: {0.436: {'width': 0.0488, 'area': 24.25661, 'height': 6.77451}}
                        }
    """
    max_area = 0
    max_wavelength = None
    max_retention = None
    for wavelength in signals:
        for retention in signals[wavelength]:
            if signals[wavelength][retention]['area'] > max_area:
                max_area = signals[wavelength][retention]['area']
                max_wavelength = wavelength
                max_retention = retention

    return max_wavelength, max_retention, max_area