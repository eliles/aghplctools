from hein_utilities.files import Watcher
from hein_utilities.misc import find_nearest, front_pad

from .hplc import HPLCTarget
from .ingestion.text import pull_hplc_area_from_txt


def pull_hplc_data_from_folder(folder, targets, wiggle=0.01, watchfor='Report.TXT'):
    """
    Pulls the HPLC integrations for all report files within the specified directory.
    This function was designed to pull all data from a given day. This method only pulls data which exists in the reports,
    which can result in asymmetric data for timepoint analysis (i.e. it assumes that subsequent runs are unrelated to
     others). If the data in a folder are for time-course analysis, use
     :py:currentmodule:`~aghplctools.hplc.pull_hplc_data_from_folder_timepoint`.


    :param folder: The folder to search for report files
    :param targets: target dictionary of the form {'name': [wavelength, retention time], ...}

    :param wiggle: the wiggle time around retention times
    :param watchfor: the name of the report file to watch
    :return: dictionary of HPLCTarget instances in the format {'name': HPLCTarget, ...}
    :rtype: dict
    """
    # todo fix to have similar functionality to v but not zero-pad
    raise NotImplementedError('This method has some fundamental flaws which need to be addressed. Please use '
                              'pull_hplc_data_from_folder_timepoint instead. ')
    targets = {  # store the targets in a vial attribute
        name: HPLCTarget(
            targets[name][0],  # wavelength
            targets[name][1],  # retention time
            name,
            wiggle=wiggle,
        ) for name in targets
    }

    files = Watcher(  # find all matching instances of the target file name
        folder,
        watchfor
    )

    for file in files:  # walk through specified path
        areas = pull_hplc_area_from_txt(file)  # pull the HPLC area
        for target in targets:  # update each target
            targets[target].add_from_pulled(areas)

    return files.contents, targets


def pull_hplc_data_from_folder_timepoint(folder, wiggle=0.02, watchfor='Report.TXT'):
    """
    Pulls all HPLC data from a folder assuming that the contents of a folder are from an ordered, time-course run (i.e.
    the contents of one report are related to the others in the folder). The method will automatically watch for new
    retention times and will prepopulate appearing values with zeros. The resulting targets will have a consistent
    number of values across the folder.

    :param folder: The folder to search for report files
    :param wiggle: the wiggle time around retention times
    :param watchfor: the name of the report file to watch
    :return: dictionary of HPLCTarget instances in the format {wavelength: {retention_time: HPLCTarget, ...}, ...}
    :rtype: dict
    """

    files = Watcher(  # find all matching instances of the target file name
        folder,
        watchfor,
    )

    targets = {}  # target storage dictionary
    filenames = []
    for file in files:  # walk across all matches
        areas = pull_hplc_area_from_txt(file)  # pull the HPLC area from file
        for wavelength in areas:  # for each wavelength
            selwl = find_nearest(  # find the appropriate wavelength in the dictionary
                targets,
                wavelength,
                1.,
            )
            if selwl is None:  # if the wavelength is not in the dictionary, create a key
                selwl = wavelength
                targets[selwl] = {}

            # currently defined targets
            current = [targets[selwl][target].retention_time for target in targets[selwl]]
            for ret in areas[selwl]:  # for each retention time in the wavelength
                selret = find_nearest(  # check for presence of retention time in targets
                    current,
                    ret,
                    wiggle,
                )
                if selret is None:  # if the retention time is not present in the targets, create target
                    targets[selwl][ret] = HPLCTarget(
                        wavelength,
                        ret,
                        wiggle=wiggle,
                    )

            for target in targets[selwl]:  # update each target
                targets[selwl][target].add_from_pulled(areas)

    total = len(files)  # total number of files
    for wavelength in targets:  # for each wavelength
        for target in targets[wavelength]:  # and each target
            # if the length of the pulled areas is less than the total number of files, front pad lists with 0.'s
            if len(targets[wavelength][target].areas) < len(files):
                targets[wavelength][target].areas = front_pad(targets[wavelength][target].areas, total)
                targets[wavelength][target].widths = front_pad(targets[wavelength][target].widths, total)
                targets[wavelength][target].heights = front_pad(targets[wavelength][target].heights, total)

    return files.contents, targets